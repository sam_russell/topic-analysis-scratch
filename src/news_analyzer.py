import os
import zipfile

import gensim
import pandas as pd

ZIP_FILEPATH = "data/NewsAggregatorDataset.zip"
DATA_FILEPATH = "data/newsCorpora.csv"


def extract_data(archive_filepath: str, data_filepath: str):
    if not os.path.exists(data_filepath):
        with zipfile.ZipFile(archive_filepath, 'r') as zip_ref:
            zip_ref.extractall("data")


def load_data(filepath: str) -> pd.DataFrame:
    df = pd.read_csv(
        filepath,
        header=0,
        names=[
            "idx",
            "text",
            "source_url",
            "publication",
            "b",
            "random_string",
            "publication_website",
            "id"
        ],
        delimiter='\t'
    )
    return df[["id", "text", "publication"]]


def read_corpus(corpus: pd.DataFrame, tokens_only=False):
    for idx, row in corpus.iterrows():
        tokens = gensim.utils.simple_preprocess(row.text)
        if not tokens_only:
            yield gensim.models.doc2vec.TaggedDocument(tokens, [idx])
        else:
            yield tokens


def train_model(corpus):
    model = gensim.models.doc2vec.Doc2Vec(
        vector_size=50,
        min_count=2,
        epochs=20
    )
    model.build_vocab(corpus)
    model.train(
        corpus,
        total_examples=model.corpus_count,
        epochs=model.epochs
    )
    return model


def test_model(model, t_data):
    output = []
    for doc_id, doc_words in enumerate(t_data, start=0):
        iv = model.infer_vector(doc_words)
        print(f"starting doc_id {doc_id}")
        try:
            sims = model.docvecs.most_similar(
                [iv], topn=10)
            print(sims)
            rank = [i for i, j in sims].index(doc_id)
        except ValueError:
            rank = []
        print(f"{doc_id}: {rank}")
        row = {
            "ranks": rank or None,
            "ranked_1st": sims[0][0],
            "ranked_2nd": sims[1][0]
        }
        output.append(row)
    return output


def test_df(t_data, corpus):
    def matched_bool(n):
        if not n and n is not None:
            return True
        else:
            return False
    t_df = pd.DataFrame(
        [
            {
                "matched": matched_bool(t_data[i]["ranks"]),
                "ranked_1st": t_data[i]["ranked_1st"],
                "ranked_2nd": t_data[i]["ranked_2nd"],
                "original_text": corpus[i].words,
                "ranked_1st_text": ",".join(
                    corpus[t_data[i]["ranked_1st"]].words),
                "ranked_2nd_text": ",".join(
                    corpus[t_data[i]["ranked_2nd"]].words)
            }
            for i in range(len(t_data))
        ]
    )
    return t_df


if __name__ == "__main__":
    extract_data(ZIP_FILEPATH, DATA_FILEPATH)
    df = load_data(DATA_FILEPATH)
    corpus_list = list(read_corpus(df))
    model = train_model(corpus_list)
    row_count = 0
    row = None
    rows = []
    it_test_data = read_corpus(df, tokens_only=True)
    while row_count < 100:
        row = it_test_data.__next__()
        row_count += 1
        rows.append(row)
    test_data = test_model(model, rows)
    test_data = test_df(test_data, corpus_list)
